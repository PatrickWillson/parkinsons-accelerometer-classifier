import numpy as np
from people import people

## Quantization

def getBorders(data, num_quantiles):
    num_dimensions = len(data[0])
    borders = []
    data = np.asarray(data)
    for d in range(num_dimensions):
        sorted_data = np.sort(data[:, d])
        boundaries = []
        sorted_data = np.array_split(sorted_data, num_quantiles)
        for i in range(len(sorted_data)-1):
            boundaries.append(np.mean([sorted_data[i][-1], sorted_data[i+1][0]]))
        boundaries.append(np.inf)
        borders.append(boundaries)
    
    return borders

def quantize(data, l1o, borders):

    num_quantiles = len(borders[0])
    num_dimensions = len(data[0])
    
    Q_pd_data = []
    Q_neg_data = []

    for p in people:
        p.Q_data = []
        for i in p.data:
            feature_vector = []
            for k in range(num_quantiles**num_dimensions):
                feature_vector.append(0)
            for j in i:
                coordinates = []
                for d in range(num_dimensions):
                    for q in range(num_quantiles):
                        if j[d] < borders[d][q]:
                            coordinates.append(q)
                            break
                index = 0
                for c in range(len(coordinates)):
                    index += coordinates[c] * num_quantiles**c
                feature_vector[index] += 1
            p.Q_data.append(feature_vector)
        if p != l1o:
            if p.status == 1:
                Q_pd_data.extend(p.Q_data)
            else:
                Q_neg_data.extend(p.Q_data)

    return Q_pd_data, Q_neg_data, l1o.Q_data

def quantizeAll(data, borders):

    num_quantiles = len(borders[0])
    num_dimensions = len(data[0])
    
    Q_pd_data = []
    Q_neg_data = []

    for p in people:
        p.Q_data = []
        for i in p.data:
            feature_vector = []
            for k in range(num_quantiles**num_dimensions):
                feature_vector.append(0)
            for j in i:
                coordinates = []
                for d in range(num_dimensions):
                    for q in range(num_quantiles):
                        if j[d] < borders[d][q]:
                            coordinates.append(q)
                            break
                index = 0
                for c in range(len(coordinates)):
                    index += coordinates[c] * num_quantiles**c
                feature_vector[index] += 1
            p.Q_data.append(feature_vector)
        if p.status == 1:
            Q_pd_data.extend(p.Q_data)
        else:
            Q_neg_data.extend(p.Q_data)

    return Q_pd_data, Q_neg_data
