import os
import shutil
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from people import getPatient, people

## READING DATA

filename = "clean_parkinsons_data"

print("reading data")

# get data from clean dataset
for f in os.listdir(filename):
    person = getPatient(f)
    if person == None:
        continue
    try:
        file = pd.read_csv(filename+"/"+f)[["mean", "absolute.deviation", "standard.deviation", "max.deviation", "PSD.1", 
        "PSD.3", "PSD.6", "PSD.10"]].to_numpy()
    except:
        continue

    #average of PSDs for file
    for i in range(len(file)):
        data = [[file[i, 0], file[i, 1], file[i, 2], file[i, 3], file[i, 4], 
        file[i, 5], file[i, 6], file[i, 7]]]
        if len(person.data) == 0:
            person.data = data
        else:
            person.data.extend(data)

for p in people:
    p.data = np.asarray(p.data)
# array with the names of all of the features
features = ["mean", "absolute.deviation", "standard.deviation", "max.deviation", "PSD.1", "PSD.3", "PSD.6", "PSD.10"]

neg_data = np.array([[0, 0]])
pd_data = np.array([[0, 0]])

## PLOTTING AND SAVING GRAPHS

filename = "graphs"

# create new directory called 'graphs'
try:
    os.mkdir(filename)
except:
    shutil.rmtree(filename)
    os.mkdir(filename)

plt.clf()

#size of points
size = 10

for i in range(len(file[0])):

    for j in range(i):

        for p in people:
            if p.status == 0:
                neg_data = np.append(neg_data, p.data[:, j:(i+1):(i-j)], 0)
            elif p.status == 1:
                pd_data = np.append(pd_data, p.data[:, j:(i+1):(i-j)], 0)

        # np.random.shuffle(pd_data)

        # try:
        #     pd_data = pd_data[0:len(neg_data)]
        # except:
        #     neg_data = neg_data[0:len(pd_data)]

        print('plotting ' + features[j] + ' vs ' + features[i])

        plt.scatter(pd_data[:, 0], pd_data[:, 1], color='red', s=0.1, alpha = 0.02)
        plt.scatter(neg_data[:, 0], neg_data[:, 1], color='blue', s=0.1, alpha = 0.02)

        # find IQR boundaries to plot graph
        data = np.append(pd_data, neg_data, 0)
        data_x = np.sort(data[:, 0])
        data_y = np.sort(data[:, 1])
        iqr_xl = data_x[2] # lower bound of IQR on x axis
        iqr_xu = data_x[int(3*len(data_x)/4)] # upper bound of IQR of y axis
        iqr_yl = data_y[2] # lower bound of IQR on y axis
        iqr_yu = data_y[int(3*len(data_x)/4)] # upper bound of IQR on y axis

        plt.xlim(iqr_xl, iqr_xu)
        plt.ylim(iqr_yl, iqr_yu)

        plt.xlabel(features[j])
        plt.ylabel(features[i])

        print("generating PNG")

        picture_name = features[j] + ' vs ' + features[i] + '.png'

        plt.savefig(filename + '/' + picture_name)

        plt.clf()
        neg_data = np.array([[0, 0]])
        pd_data = np.array([[0, 0]])


try:
    os.mkdir(filename + "/people")
except:
    shutil.rmtree(filename+"/people")
    os.mkdir(filename+"/people")

for p in people:

    if p.status == 1:
        colour = 'red'
    else:
        colour = 'blue'

    for i in people:
        if i.status == 0:
            neg_data = np.append(neg_data, i.data[:, 3:(6+1):(6-3)], 0)
        elif p.status == 1:
            pd_data = np.append(pd_data, i.data[:, 3:(6+1):(6-3)], 0)

    print('plotting ' + p.name)

    plt.scatter(p.data[:, 3], p.data[:, 6], color=colour, s=size)

    # find IQR boundaries to plot graph
    data = np.append(pd_data, neg_data, 0)
    data_x = np.sort(data[:, 0])
    data_y = np.sort(data[:, 1])
    iqr_xl = data_x[2] # lower bound of IQR on x axis
    iqr_xu = data_x[int((3*len(data_x))/4)] # upper bound of IQR of y axis
    iqr_yl = data_y[2] # lower bound of IQR on y axis
    iqr_yu = data_y[int((3*len(data_y))/4)] # upper bound of IQR on y axis

    plt.xlim(iqr_xl, iqr_xu)
    plt.ylim(iqr_yl, iqr_yu)

    plt.xlabel(features[3])
    plt.ylabel(features[6])

    print("generating PNG")

    picture_name = p.name + '.png'

    plt.savefig(filename + '/people/' + picture_name)

    plt.clf()
    neg_data = np.array([[0, 0]])
    pd_data = np.array([[0, 0]])
