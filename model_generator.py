import os
import pandas as pd
import numpy as np
import pickle
from sklearn.ensemble import RandomForestClassifier
from joblib import dump

from people import getPatient, people
from quantization import getBorders, quantizeAll

# initailse random forest classifier
model = RandomForestClassifier(n_estimators=2500, criterion='entropy')

filename = "clean_parkinsons_data"
num_quantiles = 4

print("Quantiles:", num_quantiles)

# get data from clean dataset
for f in os.listdir(filename):
    person = getPatient(f)
    if person == None:
        continue
    try:
        file = pd.read_csv(filename+"/"+f)[["mean", "absolute.deviation", "standard.deviation", "max.deviation", "PSD.1", 
        "PSD.3", "PSD.6", "PSD.10"]].to_numpy()
    except:
        continue

    #average of PSDs for file
    data = []
    for i in range(len(file)):
        data.append([file[i, 2], file[i, 6]])
    person.data.append(data)

print("All files loaded") 

# cross validation
data = []
results = []

neg_data = []
pd_data = []


for p in people:
    person_data = []
    for i in p.data:
        person_data.extend(i)
    if p.status == 0:
        neg_data.extend(person_data)
    elif p.status == 1:
        pd_data.extend(person_data)

    data.extend(pd_data)
    data.extend(neg_data)

borders = getBorders(data, num_quantiles)
Q_pd_data, Q_neg_data = quantizeAll(data, borders)

Q_data = []
Q_results = []

np.random.shuffle(Q_pd_data)
# np.random.shuffle(Q_neg_data)

try:
    Q_pd_data = Q_pd_data[0:len(Q_neg_data)]
except:
    Q_neg_data = Q_neg_data[0:len(Q_pd_data)]

Q_data.extend(Q_pd_data)
Q_data.extend(Q_neg_data)

for i in range(len(Q_pd_data)):
    Q_results.append(1)
for i in range(len(Q_neg_data)):
    Q_results.append(0)

predictions = []
model.fit(Q_data, Q_results)

print("Borders: ", borders)

dump(model, 'model.joblib')

print("done")