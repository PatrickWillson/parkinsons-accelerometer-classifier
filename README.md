# Parkinsons Accelerometer Classifier

Classifier for Parkinson's disease through machine learning with smartphone accelerometer data

Please run preprocessing.py before running q_model_testing.py, plot_feature_graphs.py or model_generator.py

This model was implemented into a framework, invloving an Android application and a Flask server, both of which can be found here:

Server: https://gitlab.com/comp3931-21_22/comp3931-server

Android Application: https://gitlab.com/comp3931-21_22/comp3931-base-mobile-app
