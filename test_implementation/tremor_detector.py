from http.client import BAD_REQUEST
from flask import request, g, jsonify, make_response
from flask_jwt_extended import (jwt_required, get_jwt_identity)
import pandas as pd
import numpy as np

from app.api import bp
from app.projects.tremor_detector.raw_data_parsing import parseAccelerometer, parseMagnetometer, parseGPS
from app.projects.tremor_detector.preprocessing import preprocessing
from app.projects.tremor_detector.tremor_recordings_model import saveFeatureVector, getFeatureVectors


@bp.route('/tremor-detector/uploadcsv/', methods=['POST'])
@jwt_required()
def uploadCSV():
    current_user = get_jwt_identity()
    data = request.get_json()
    if current_user != None:
        accel = parseAccelerometer(data['accelerometer'])
        compass = parseMagnetometer(data['magnetometer'])
        gps = parseGPS(data['gps'])
        featureVector = preprocessing(accel, compass, gps)
        saveFeatureVector(featureVector, current_user)
    else:
        response = make_response("Please Login", 401)
        response.mimetype = "text/plain"
        return response

@bp.route('/tremor-detector/getinstances', methods=['GET'])
@jwt_required()
def getInstances():
    current_user = get_jwt_identity()
    if current_user != None:
        return str(len(getFeatureVectors(current_user)))
    else:
        return 'Not logged in'