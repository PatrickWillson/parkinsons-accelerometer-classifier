import os
import numpy as np
import pandas as pd
from joblib import load
from sklearn.ensemble import RandomForestClassifier

from raw_data_parsing import parseAccelerometer, parseMagnetometer, parseGPS
from preprocessing import preprocessing

data = "data"

accel = ""
compass = ""
gps = ""

def convertToString(array):
    
    try:

        array = array.tolist()

        output = ""

        for i in range(len(array)):
            array[i] = ','.join([str(j) for j in array[i]])
            output += array[i] + '\n'

        return output
    except:
        return ""

for filename in os.listdir(data):
    if "accelerometer" in filename.lower():
        df = pd.read_csv(data+'/'+filename)
        accel = df[['timestamp', 'x', 'y', 'z']].to_numpy()
        
        for i in range(len(accel)):
            accel[i][0] = accel[i][0].split(' ')[1].split('.')[0]
        
    elif "magnetometer" in filename.lower():
        df = pd.read_csv(data+'/'+filename)
        compass = df[['timestamp', 'x', 'y', 'z']].to_numpy()
        
        for i in range(len(compass)):
            compass[i][0] = compass[i][0].split(' ')[1].split('.')[0]
    
    elif "gps" in filename.lower():
        df = pd.read_csv(data+'/'+filename)
        gps = df[['timestamp', 'latitude', 'longitude', 'altitude']].to_numpy()
        
        for i in range(len(gps)):
            gps[i][0] = gps[i][0].split(' ')[1].split('.')[0]

accel = parseAccelerometer(convertToString(accel))
compass = parseMagnetometer(convertToString(compass))
gps = parseGPS(convertToString(gps))

featureVector = preprocessing(accel, compass, gps)

print(featureVector)

if featureVector != None:

    model = load('../model.joblib')

    print(model.predict([featureVector]))
