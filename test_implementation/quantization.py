import numpy as np

borders = [[0.0352643479134358, 0.0516270518404265, 0.1511272102644022, np.inf], [0.07227331269126461, 0.1811892585160951, 1.8459970494567, np.inf]]

def quantization(data):
    global borders

    num_quantiles = len(borders[0])
    num_dimensions = len(data[0])
    
    feature_vector = []
    for k in range(num_quantiles**num_dimensions):
        feature_vector.append(0)
    for j in data:
        coordinates = []
        for d in range(num_dimensions):
            for q in range(num_quantiles):
                if j[d] < borders[d][q]:
                    coordinates.append(q)
                    break
        index = 0
        for c in range(len(coordinates)):
            index += coordinates[c] * num_quantiles**c
        feature_vector[index] += 1
    
    return feature_vector
    