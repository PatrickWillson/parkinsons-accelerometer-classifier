class Person():

    def __init__(self, name, id, status, sex):
        self.name = name
        self.id = id
        self.status = status
        self.sex = sex
        self.data = []
        self.q_data = []
        self.accuracy = 0

#declaring people
#1 represents male, 0 represents female
apple = Person("Apple", "APPLE", 0, 1)
cherry = Person("Cherry", "CHERRY", 1, 0)
crocus = Person("Crocus" , "CROCUS", 1, 1)
dafodil = Person("Dafodil", "DAFODIL", 0, 1)
daisy = Person("Daisy", "DAIS", 1, 1)
flox = Person("Flox", "FLOX", 1, 1)
iris = Person("Iris", "IRIS", 1, 1)
lily = Person("Lily", "LIL", 0, 0)
maple = Person("Maple", "MAPLE", 1, 1)
orange = Person("Orange", "ORANGE", 0, 1)
orchid = Person("Orchid", "ORCHID", 1, 1)
peony = Person("Peony", "PEONY", 1, 1)
rose = Person("Rose", "ROSE", 0, 1)
sunflower = Person("Sunflower", "SUNFLOWER", 0, 1)
sweetpea = Person("Sweetpea", "SWEETPEA", 0, 0)
violet = Person("Violet", "VIOLET", 1, 0)

people = [apple, cherry, crocus, dafodil, daisy, flox, iris, lily, maple, orange, orchid, peony, rose, sunflower, sweetpea, violet]
negatives = [apple, dafodil, lily, orange, rose, sunflower, sweetpea]
pds = [cherry, crocus, daisy, flox, iris, maple, orchid, peony, violet]

def getPatient(filename):
    for p in people:
        if p.id in filename:
            return p
    return None