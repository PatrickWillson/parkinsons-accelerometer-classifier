import numpy as np
import os
import shutil
import pandas as pd
from operator import itemgetter
from sklearn.cluster import KMeans
import datetime as dt
from people import getPatient

dataset = "raw_parkinsons_data"
new_dataset = "clean_parkinsons_data"

file_length = 600
min_file_length = 120
num_clusters = 10

time_id = 0
time_dict = {}

try:
    os.mkdir(new_dataset)
except:
    shutil.rmtree(new_dataset)
    os.mkdir(new_dataset)

def calculateRMS(array):
    return np.sqrt(np.mean(np.square(array)))

# assign int ID to time value
def assignTimeID(timeString):
    global time_id, time_dict
    if timeString in time_dict:
        return time_dict[timeString]
    time_dict[timeString] = time_id
    time_id += 1
    return time_id-1

def timeToInt(timeString):
    return float(timeString.split(' ')[1].replace(':', ''))

# binary search to find instance of specific time in array
def findTime(time, array):
    index = int(len(array)/2)
    array_time = timeToInt(array[index][-1])
    if array_time == time:
        return array[index]
    elif index == 0:
        return [None]
    elif array_time > time:
        return findTime(time, array[0:index])
    elif array_time < time:
        return findTime(time, array[index:])

count = 0

for filename in os.listdir(dataset):

    time_dict = {}
    time_id = 0

    count += 1

    print("cleaning " + str(count) + "/5594")

    patient = getPatient(filename)

    #variables to store array of sensor data
    accel = np.array([None])
    compass = np.array([None])
    gps = np.array([None])
    for f in os.listdir(dataset+"/"+filename):
        df = pd.read_csv(dataset+"/"+filename+"/"+f)

        # adds accelerometer data to array
        if "accel" in f:
            try:
                if accel.any() == None:
                    accel = df[["x.mean", "x.absolute.deviation", "x.standard.deviation", "x.max.deviation", "x.PSD.1", "x.PSD.3", "x.PSD.6",
                    "x.PSD.10", "y.mean", "y.absolute.deviation", "y.standard.deviation", "y.max.deviation", "y.PSD.1", "y.PSD.3", "y.PSD.6", 
                    "y.PSD.10", "z.mean", "z.absolute.deviation", "z.standard.deviation", "z.max.deviation", "z.PSD.1", "z.PSD.3", "z.PSD.6", 
                    "z.PSD.10", "diffSecs", "N.samples", "time"]].to_numpy()
                else:
                    accel = np.append(accel, df[["x.mean", "x.absolute.deviation", "x.standard.deviation", "x.max.deviation", "x.PSD.1", 
                    "x.PSD.3", "x.PSD.6","x.PSD.10", "y.mean", "y.absolute.deviation", "y.standard.deviation", "y.max.deviation", "y.PSD.1", 
                    "y.PSD.3", "y.PSD.6", "y.PSD.10", "z.mean", "z.absolute.deviation", "z.standard.deviation", "z.max.deviation", "z.PSD.1", 
                    "z.PSD.3", "z.PSD.6", "z.PSD.10", "diffSecs", "N.samples", "time"]].to_numpy(), 0)
            except:
                continue
 
        # adds compass data to array
        elif "cmpss" in f:
            try:
                if compass.any() == None:
                    compass = df[["azimuth.mean", "azimuth.absolute.deviation", "azimuth.standard.deviation", "azimuth.max.deviation", 
                    "pitch.mean", "pitch.absolute.deviation", "pitch.standard.deviation", "pitch.max.deviation", "roll.mean", "roll.absolute.deviation",	
                    "roll.standard.deviation", "roll.max.deviation", "time"]].to_numpy()
                else:
                    compass = np.append(compass, df[["azimuth.mean", "azimuth.absolute.deviation", "azimuth.standard.deviation", "azimuth.max.deviation", 
                    "pitch.mean", "pitch.absolute.deviation", "pitch.standard.deviation", "pitch.max.deviation", "roll.mean", "roll.absolute.deviation",	
                    "roll.standard.deviation", "roll.max.deviation", "time"]].to_numpy(), 0)
            except:
                continue

        # adds gps data to array
        elif "gps" in f:
            if gps.any() == None:
                gps = df[["latitude", "longitude", "altitude", "time"]].to_numpy()
            else:
                gps = np.append(gps, df[["latitude", "longitude", "altitude", "time"]].to_numpy(), 0)

    if len(accel) < file_length:
        continue
    
    # array of instances to cluster
    all_instances = []
    instances = []

    # aggregate based on times
    for a in range(len(accel)):
        try:
            time = accel[a][-1]
        except:
            # accel = np.delete(accel, a)
            continue

        # remove bad readings from the results
        if accel[a][-2] < 35 or accel[a][-3] > 1.1 or accel[a][-3] < 0.9:
            accel = np.delete(accel, a, axis=0)
            continue
        if len(compass) >= file_length:
            found = findTime(timeToInt(time), compass)
            if len(found) <= 1:
                continue
            all_instances.append([calculateRMS(accel[a][2:24:8]), calculateRMS(found[2:12:4])])
        else:
            instances = []
            break
        if len(gps) >= file_length:
            found = findTime(timeToInt(time), gps)
            if len(found) <= 1:
                continue
            instances.append(all_instances[-1])
            instances[-1].extend([found[0], found[1], found[2]])
        else:
            instances = all_instances

        instances[-1].extend([assignTimeID(time)])
    
    if len(instances) < file_length:
        continue

    # array of times of instances in each cluster
    times = []
    classes = []
    for i in range(num_clusters):
        times.append([])
        classes.append([])

    # cluster data
    cluster = KMeans(n_clusters=len(classes), random_state=0).fit_predict(instances[:])

    # classes not to be included
    outlier_classes = []

    # sort times of instances in clusters
    for i in range(len(cluster)):
        classes[cluster[i]].append(instances[i][0:-1])
        times[cluster[i]].append(instances[i][-1])      
    
    for c in range(len(classes)):
        # classes are outliers if:

        # if classes have fewer than a number of  instances
        if len(classes[c]) < min_file_length:
            outlier_classes.append(c)
            continue

        # if standard deviation of accelerometer is too small or standard deviation compass is too large
        if np.median(classes[c][0]) < 0.02 or np.median(classes[c][1]) > 2.5:
            outlier_classes.append(c)
            continue

        classes[c] = np.asarray(classes[c])

        try:
            # if gps location is too far away from median
            med_x = np.median(classes[c][:, 2])
            med_y = np.median(classes[c][:, 3])
            med_z = np.median(classes[c][:, 4])
            for g in classes[c]:
                if calculateRMS([g[2] - med_x, g[3] - med_y, g[4] - med_z]) > 20:
                    outlier_classes.append(c)
                    break
        except:
            pass

    to_export = []

    # for i in range(num_clusters):
    #     to_export.append([])

    for a in accel:
        for t in range(len(times)):
            if assignTimeID(a[-1]) in times[t] and t not in outlier_classes:
                to_export.append([calculateRMS(a[0:20:8]), calculateRMS(a[1:21:8]), calculateRMS(a[2:22:8]), 
                calculateRMS(a[3:23:8]), calculateRMS(a[4:24:8]), calculateRMS(a[5:24:8]), calculateRMS(a[6:24:8]), 
                calculateRMS(a[7:24:8])])
                break
    saved = False
    # for i in range(len(to_export)):
    if len(to_export) >= file_length:
        saved = True
        df = pd.DataFrame(to_export, columns=["mean", "absolute.deviation", "standard.deviation", "max.deviation", "PSD.1", 
        "PSD.3", "PSD.6", "PSD.10"])
        df.to_csv(new_dataset+'/'+filename+"_"+str(i)+".csv")
    if saved:
        print("   saved")

