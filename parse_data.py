import os
import numpy as np
import pandas as pd
from scipy.signal import welch

dataset = "raw_data_to_parse"
export_dataset = "feature_dataset"

def calculateRMS(array):
    return np.sqrt(np.mean(np.square(array)))

# create export dataset if it doesn't already exist
try:
    os.mkdir(export_dataset)
except:
    pass

for filename in os.listdir(dataset):

    accel = []
    compass = []
    gps = []

    for f in os.listdir(dataset+"/"+filename):
        df = pd.read_csv(dataset+"/"+filename+"/"+f)
        if "Accelerometer" in f:
            accel = df[['timestamp', 'x', 'y', 'z']].to_numpy()
        elif "Magnetometer" in f:
            compass = df[['timestamp', 'x', 'y', 'z']].to_numpy()

    prev_time = -1

    current_data = []

    table = []

    for a in accel:

        time = int(a[0].split(' ')[1].split(':')[2].split('.')[0])

        if time == prev_time:

            # format time string and append data to current data array
            formatted_time = a[0].split('.')[0].replace('-', '/')
            current_data.append([formatted_time, float(a[1]), float(a[2]), float(a[3])])

        else:
            prev_time = time
            if len(current_data) > 0:

                current_data = np.asarray(current_data)
                row = []
                std = []
                std.append(np.std(current_data[:, 1].astype(float))) # x.standard.deviation
                std.append(np.std(current_data[:, 2].astype(float))) # y.standard.deviation
                std.append(np.std(current_data[:, 3].astype(float))) # z.standard.deviation

                row.append(current_data[-1][0].split(' ')[1])
                row.append(calculateRMS(std))

                #calculate psd
                powerSpec = []
                for i in range(1,4):
                    freq, psd = welch(current_data[:, 1].astype(float), fs=40, nperseg=len(current_data))
                    
                    prev_freq = 0

                    psd6 = 0
                    for f in range(len(freq)):
                        if freq[f] < 6:
                            prev_freq = f
                        elif freq[f] == 6:
                            psd6 = psd[f]
                            break
                        else:
                            diff = psd[f] - psd[prev_freq]
                            psd6 = psd[prev_freq] + (diff *(6-freq[prev_freq])/((6-freq[prev_freq])+(freq[f]-6)))
                            break
                    powerSpec.append(psd6)
                
                row.append(calculateRMS(powerSpec))
                
                table.append(row)

                current_data = []

print(table)
