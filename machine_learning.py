import os
from operator import itemgetter
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

from people import getPatient, people

# initialise RBF kernel and SVM classifier
# kernel = RBF(1.0)
# model = svm.NuSVC(nu=0.8, kernel=kernel)

# model = svm.SVC(kernel = kernel)
model = RandomForestClassifier()
# model = KNeighborsClassifier(n_neighbors = 10)

filename = "clean_parkinsons_data"

# get data from clean dataset
for f in os.listdir(filename):
    person = getPatient(f)
    if person == None:
        continue
    try:
        file = pd.read_csv(filename+"/"+f)[["mean", "absolute.deviation", "standard.deviation", "max.deviation", "PSD.1", 
        "PSD.3", "PSD.6", "PSD.10"]].to_numpy()
    except:
        continue

    #average of PSDs for file
    data = [np.mean(file[:, 2]), np.mean(file[:, 6]), person.status]
    person.data.append(data)

print("All files loaded") 
    
tp = []
tn = []

num_correct = 0

#leave one person out for testing
for l1o in people:
    
    # cross validation
    data = []
    results = []

    neg_data = []
    pd_data = []


    for p in people:
        person_data = []
        for i in p.data:
            person_data.append(i)
        if p.status == 0 and p != l1o:
            neg_data.extend(person_data)
        elif p.status == 1 and p != l1o:
            pd_data.extend(person_data)

        data.extend(pd_data)
        data.extend(neg_data)

    # np.random.shuffle(pd_data)
    # np.random.shuffle(neg_data)

    # try:
    #     data.extend(pd_data[0:len(neg_data)])
    #     data.extend(neg_data)
    # except:
    #     data.extend(neg_data[0:len(pd_data)])
    #     data.extend(pd_data)

    for d in range(len(data)):
        results.append(data[d][-1])
        data[d] = data[d][0:-1]


    # data.extend(neg_data)
    # data.extend(pd_data)
    # data = np.asarray(data)

    predictions = []

    model.fit(data, results)
    # if model.predict([l1o.Q_data]) == [l1o.status]:
    #     predictions.append(1)
    # else:
    #     predictions.append(0)
    for i in l1o.data:
        i = i[0:-1]
        if model.predict([i]) == [l1o.status]:
            predictions.append(1)
        else:
            predictions.append(0)
    if l1o.status == 1:
        tp.append(np.mean(predictions))
    else:
        tn.append(np.mean(predictions))
    if np.mean(predictions) > 0.5:
        num_correct += 1
    result = np.mean(predictions)
    if l1o.status == 0:
        result = 1-result
    print(l1o.name, result * 100, "             Class: ", l1o.status)
    
tp.extend(tn)
print("Average: ", np.mean(tp)*100)
print(str(num_correct)+"/16")
